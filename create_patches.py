#!/usr/local/bin/python
import pyotb
import geopandas as gpd
from dinamis_sdk import sign_inplace
import pystac_client
import os
import argparse

##################### parameters en entree ############################
parser = argparse.ArgumentParser(description="Application pour extraire les patches")
parser.add_argument("--year", required=True, type=int, help="Année (entier)")
parser.add_argument("--vec_point", required=True, type=str, help="Couche vecteur points")
parser.add_argument("--vec_bati", required=True, type=str, help="Couche vecteur bati")
parser.add_argument("--out_dir", required=True, type=str, help="Dossier en sortie")
parser.add_argument("--vec_points_field", default="id", help="Champ requis pour points")
parser.add_argument("--vec_bati_class", default="class_int", help="Champ requis pour bati (classe) doit etre un champ d'entiers")
params = parser.parse_args()

year = params.year
vec_point = params.vec_point
vec_bati = params.vec_bati

# nom du champ (quelconque) pour le nuage de points
# (il en faut au moins un)
vec_points_field = params.vec_points_field

# les polygons de bati doivent avoir un champ qui porte 
# le numero de la classe
# on peut en creer un nouveau dans QGIS avec l'expression:
#   if("CLASS"= 'bati_photo', 2, if("CLASS"= 'bati' ,1,0))
vec_bati_class = params.vec_bati_class

# taille des patches
patch_size = 128

##################### Partie recherche STAC ############################
api = pystac_client.Client.open(
   'https://stacapi-cdos.apps.okd.crocc.meso.umontpellier.fr',
)

# bbox points
roi = gpd.read_file(vec_point)
bbox = roi.to_crs(4326).total_bounds

# recherche stac
results = api.search(
  collections=["spot-6-7-drs"],
  datetime=f"{year}-01-01/{year}-12-30",
  bbox=bbox
)

################### Partie extraction des patches #######################
for item in results.items():

  # output directory
  out_dir = os.path.join(params.out_dir, f"{item.id}_{patch_size}")
  if not os.path.exists(out_dir):
    os.makedirs(out_dir)

  # spot-6/7 images
  pan = sign_inplace(item.assets["src_pan"].href)
  xs = sign_inplace(item.assets["src_xs"].href)
  
  print(f"Pan: {pan}")
  
  # rasterization
  labels = pyotb.Rasterization({
    "in": vec_bati,
    "im": pan,
    "mode": "attribute",
    "mode.attribute.field": vec_bati_class
  })

  try:  
    # patches extraction
    patches = pyotb.PatchesExtraction(
      frozen=True,
      n_sources=3,  # Tells the OTB application to use 3 sources
      source1_il=pan,
      source1_patchsizex=patch_size,
      source1_patchsizey=patch_size,
      source1_nodata=0,
      source2_il=xs,
      source2_patchsizex=int(patch_size / 4),
      source2_patchsizey=int(patch_size / 4),
      source2_nodata=0,
      source3_il=labels,
      source3_patchsizex=patch_size,
      source3_patchsizey=patch_size,
      vec=vec_point,
      field=vec_points_field
    )
    
    # write
    out_dict = {
      "source1.out": os.path.join(out_dir, "pan.tif"),
      "source2.out": os.path.join(out_dir, "xs.tif"),
      "source3.out": os.path.join(out_dir, "tt.tif"),
    }
    pixel_type = {
      "source1.out": "int16",
      "source2.out": "int16",
      "source3.out": "uint8",
    }
    ext_fname = "gdal:co:COMPRESS=DEFLATE"

    patches.write(out_dict, pixel_type=pixel_type, ext_fname=ext_fname)
  except RuntimeError as e:
    print(f"No patch written. Maybe no patch available on this image? ({e})")
  
