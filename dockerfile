FROM mdl4eo/otbtf:4.3.1-cpu
RUN pip install dinamis_sdk pystac_client pyotb geopandas
COPY startup_message /tmp/startup_message
RUN cat /tmp/startup_message >> $HOME/.profile
COPY --chown=otbuser create_patches.py /home/otbuser/bin/create_patches.py
#USER root
RUN chmod +x /home/otbuser/bin/create_patches.py
RUN export PATH=$PATH:/home/otbuser/bin/
ENTRYPOINT ["/bin/bash", "-l"]
