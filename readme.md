# Pipeline de génération des Patches

## Parametres de l'application

- `year`: numéro de l'année, valeur entière
- `vec_point`: chemin d'accès vers la couche vectorielle des points
- `vec_bati`: chemin d'accès vers la couche vectorielle du bati
- `out_dir`: chemin d'accès vers le dossier dans lequel les patches sont écrits

## Données en entrée

- **couche vectorielle pour les points**: :warning: doit avoir un champ quelconque dans la table attributaire. Ce champ est **id** par défaut dans l'application (cela peut être changé avec `--vec_point_field`).
- **couche vectorielle pour les géométries de bati**: :warning: doit avoir un champ avec des valeurs étant des **nombres entier** pour la classe (0: autre, 1: bati, 2: bati avec panneaux). Ce champ est *+*class_int** par défaut dans l'application (cela peut être changé avec `--vec_bati_class`)

## Utiliser l'appli

```commandLine
create_patches.py
  --year 2021
  --vec_point /data/data_test/Patches_Points.shp
  --vec_bati /data/data_test/bati.geojson
  --out_dir /data/output/
```
